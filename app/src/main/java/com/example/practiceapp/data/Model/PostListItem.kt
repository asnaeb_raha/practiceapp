package com.example.practiceapp.data.Model

data class PostListItem(
    val body: String,
    val id: Int,
    val title: String,
    val userId: Int
)