package com.example.practiceapp.data.API

import com.example.practiceapp.data.Model.PostList
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface ApiInterface {

    //posts
    @GET("/posts")
    fun getPosts() : Call<PostList>


    //get the api interface
    companion object{
        fun getRetrofitInstance(base_url:String): ApiInterface? {
            return Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(base_url)
                .build().create(ApiInterface::class.java)
        }
    }
}