package com.example.practiceapp.UI.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.practiceapp.R
import com.example.practiceapp.data.Model.PostListItem

class RVAdapter(var todoList: ArrayList<PostListItem>) : RecyclerView.Adapter<RVAdapter.PostListVH>() {
    class PostListVH(itemView : View) : RecyclerView.ViewHolder(itemView){
        var titleText: TextView = itemView.findViewById(R.id.titleText)
        var bodyText:TextView = itemView.findViewById(R.id.bodyText)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostListVH {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.post_layout, parent, false)
        return PostListVH(view)
    }

    override fun onBindViewHolder(holder: PostListVH, position: Int) {
        val currPostData = todoList[position]
        holder.apply {
            titleText.text = currPostData.title
            bodyText.text = currPostData.body
        }
    }

    override fun getItemCount(): Int {
        return  todoList.count()
    }
}