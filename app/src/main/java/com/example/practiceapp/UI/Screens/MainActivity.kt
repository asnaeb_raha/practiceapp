package com.example.practiceapp.UI.Screens

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.practiceapp.R
import com.example.practiceapp.UI.Adapters.RVAdapter
import com.example.practiceapp.data.API.ApiInterface
import com.example.practiceapp.data.Model.PostList
import com.example.practiceapp.data.Model.PostListItem
import com.example.practiceapp.data.Util
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    private lateinit var postList:RecyclerView
    private lateinit var postListData:ArrayList<PostListItem>
    private var TAG = "ERROR ENCOUNTERED***"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        postList = findViewById(R.id.postsList)
        postList.layoutManager = LinearLayoutManager(this)
    }

    override fun onStart() {
        super.onStart()

        val interfaceInstance = ApiInterface.getRetrofitInstance(Util().base_url)
        postListData = ArrayList()
        getData(interfaceInstance)
    }

    private fun getData(apiInterface: ApiInterface?){
        val postData = apiInterface?.getPosts()
        postData?.enqueue(object: Callback<PostList>{
            override fun onResponse(call: Call<PostList>?, response: Response<PostList>?) {

                val responseBody = response!!.body()
                if(response.isSuccessful){
                    for(item in responseBody){
                        postListData.add(item)
                    }

                    postList.adapter = RVAdapter(postListData)
                }

            }

            override fun onFailure(call: Call<PostList>?, t: Throwable?) {
                Log.i(TAG, t?.localizedMessage.toString())
            }

        })
    }
}